import React from "react";
import { Route, Switch } from "react-router-dom";
import { HOME_URL } from "./../../data/constants/urls";


const Routes: React.FunctionComponent<any> = () => (
  <div>
    <Switch>
      <Route path={HOME_URL} exact />
    </Switch>
  </div>
);

export default Routes;
