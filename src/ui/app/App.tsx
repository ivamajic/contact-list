import * as React from "react";
import { Provider } from "react-redux";
import { BrowserRouter } from "react-router-dom";
import { store } from "../../state/store";
import Routes from "./Routes";

const AppContainer: React.FunctionComponent = () => (
  <Provider store={store}>
    <BrowserRouter>
      <Routes/>
    </BrowserRouter>
  </Provider>
);

export default AppContainer;
