/* eslint-disable @typescript-eslint/explicit-module-boundary-types */
/* eslint-disable @typescript-eslint/no-unsafe-return */
import { combineReducers, Reducer } from "redux";
import { ReduxAction } from "../types";

const reducers: Reducer = combineReducers({
});

const rootReducer = (state: any, action: ReduxAction) => reducers(state, action);

export type AppState = ReturnType<typeof rootReducer>;
export default rootReducer;
